import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import VueGoogleCharts from 'vue-google-charts'
import {i18n} from './plugins/i18n'
import FlagIcon from 'vue-flag-icon'
import VeLine from 'v-charts/lib/line.common'
Vue.component(VeLine.name, VeLine)
Vue.use(VueGoogleCharts)


Vue.config.productionTip = false

Vue.config.productionTip = false;
Vue.use(FlagIcon)

new Vue({
  router,
  i18n,
  render: h => h(App)
}).$mount('#app')
