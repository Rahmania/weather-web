import Vue from 'vue';
import VueI18n from 'vue-i18n';

Vue.use(VueI18n)
export const i18n = new VueI18n({
                                                  locale:'en',
                                                  fallbackLocale:'de',
 messages : {
    'en': {
        climate: 'German climate is rarely extreme, there are occasional spikes of cold or heat. Winter temperatures can sometimes drop to two-digit negative temperatures for a few days in a row. Conversely, summer can see periods of very high temperatures for a week or two. The recorded extremes are a maximum of 40.3 °C (104.5 °F) (July 2015, in Kitzingen), and a minimum of −37.8 °C',
        barchart: 'A bar chart or bar graph is a chart or graph that presents categorical data with rectangular bars with heights or lengths proportional to the values that they represent. The bars can be plotted vertically or horizontally. A vertical bar chart is sometimes called a line graph.',
        checkout: 'Germany Weather',
        plugins: 'Bar Graph',
        links: 'Current Weather',
        ecosystem: 'WEATHER',
        des:'Description'
    },
    'de': {
        climate: 'Das deutsche Klima ist selten extrem, es gibt gelegentlich Kälte- oder Wärmespitzen. Die Wintertemperaturen fallen manchmal für einige Tage hintereinander auf zweistellige Minustemperaturen. Umgekehrt können im Sommer für ein oder zwei Wochen sehr hohe Temperaturen auftreten. Die aufgezeichneten Extreme liegen bei maximal 40,3 ° C (Juli 2015 in Kitzingen) und minimal -37,8 ° C',
        barchart: 'Ein Balkendiagramm oder ein Balkendiagramm ist ein Diagramm oder ein Diagramm, das kategoriale Daten mit rechteckigen Balken darstellt, deren Höhe oder Länge proportional zu den von ihnen dargestellten Werten ist. Die Balken können vertikal oder horizontal gezeichnet werden. Ein vertikales Balkendiagramm wird manchmal als Liniendiagramm bezeichnet.',
        checkout: 'Deutschland Wetter',
        plugins: 'Balkendiagramm',
        links: 'Aktuelle Wetterlage',
        ecosystem: 'WETTER',
        des:'Beschreibung'
    }
}
})


export default i18n;