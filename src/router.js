import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/views/Dashboard.vue'
import Projects from '@/views/Projects'
import Team from '@/views/Team'
import Home from '@/views/home'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [

    {
      path: '/',
      name: 'Home',
      component: Home
    },
   
    {
      path: '/lineChart',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path:'/Projects',
      name:'project',
      component:Projects
      
    },
    {
      path:'/BarChart',
      name:'team',
      component:Team
      
    }
  ]
})
